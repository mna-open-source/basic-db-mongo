# MongoDB

## Prerequisite Software

Before you can work with this project, you must install and configure the following products on your development machine:

- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) on Mac computers, it's easier to install it using Homebrew: `brew install awscli`
- [Git](http://git-scm.com) and/or the **GitHub app** (for [Mac](http://mac.github.com) or [Windows](http://windows.github.com))
- [Docker](https://docs.docker.com/engine/install/)

## Run Docker Compose

Run the docker container with following command:

```shell
docker-compose up
```

To stop all services

```shell
docker-compose down
```


## Resources

- [Docker](https://www.docker.com/)
- [Managing MongoDB on docker with docker-compose](https://faun.pub/managing-mongodb-on-docker-with-docker-compose-26bf8a0bbae3)
